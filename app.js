var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const port = 8000;

app.get('/palindrome', function(req,res) {
    var value = req.query.palindrome;

    // Masukkan Palindrome disini
    var palindrome = (str) => {
        var len = str.length;
        var mid = Math.floor(len/2);

        for ( var i = 0; i < mid; i++ ) {
            if (str[i] !== str[len - 1 - i]) {
                return false;
            }
        }

        return true;
    }

    var result = palindrome(value);

    return res.status(200).send({
        status: 'OK',
        value: result
    });
})

http.createServer(app).listen(port);

console.log("Server started on port " + port);

module.exports = app;